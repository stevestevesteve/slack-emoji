# **slack emoji**
<img src="./color-computer.gif"/> <img src="./dancing-cactus.gif"/>

Some lovely custom emoji for your Slack team.

- <img src="./color-computer.gif" width="32px"/> `:color-computer:`
- <img src="./dancing-cactus.gif" width="32px"/> `:dancing-cactus:`
- <img src="./glorp.gif" width="32px"/> `:glorp:`
